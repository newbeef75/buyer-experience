---
  title: Platform
  description: Learn more about how the GitLab platform can help teams collaborate and build software faster.

  header: A comprehensive software innovation platform
  img:
    url: /nuxt-images/platform/devops-infinity-shield.png
    alt: The DevOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield.

  cards:
    - icon: ebook
      type: Ebook
      text: Starting and Scaling DevOps in the Enterprise
      link:
        href: https://about.gitlab.com/resources/scaling-enterprise-devops/
        data_ga_name: starting and scaling devops in the enterprise ebook
        data_ga_location: platform cards
    - icon: blog
      type: Blog Post
      text: Making the case for a DevOps platform
      link:
        href: https://about.gitlab.com/blog/2021/09/08/making-the-case-for-a-devops-platform-what-data-and-customers-say/
        data_ga_name: making the case for a devops platform blog post
        data_ga_location: platform cards
    - icon: whitepapers
      type: White Paper
      text: Manage your toolchain before it manages you
      link:
        href: https://about.gitlab.com/resources/whitepaper-forrester-manage-your-toolchain/
        data_ga_name: manage your toolchain before it manages you white paper
        data_ga_location: platform cards

  platform_features:
    table_label: GitLab features that fall within each stage of the software development lifecycle. These stages are grouped into phases of software innovation.
    manage_table:
      mature:
        - title: Authentication & Authorization |
          link:
        - title: Value stream management |
          link: https://about.gitlab.com/solutions/value-stream-management/
        - title: User management
          link:
      early:
        - title: DevOps reports |
          link: https://docs.gitlab.com/ee/user/admin_area/analytics/dev_ops_report.html
        - title: Credential management |
          link:
        - title: Permissions
          link:
    main_table:
      - stages:
          - name: Plan
            features:
              - name: Portfolio Management
                key: mature
                link: https://about.gitlab.com/stages-devops-lifecycle/portfolio-management/
              - name: Team Planning
                key: mature
                link: https://docs.gitlab.com/ee/topics/plan_and_track.html?_gl=1*1qibz1u*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY0NjYyNC4xNTkuMC4xNjY2NjQ2NjQ0LjAuMC4w#team-planning
              - name: Service desk
                key: mature
                link: https://about.gitlab.com/stages-devops-lifecycle/service-desk/
              - name: Design management
                key: mature
                link: https://docs.gitlab.com/ee/user/project/issues/design_management.html?_gl=1*1qibz1u*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY0NjYyNC4xNTkuMC4xNjY2NjQ2NjQ0LjAuMC4w
              - name: (DORA)
                key: mature
              - name: Planning analytics
                key: early
              - name: Requirements management
                key: early
                link: https://docs.gitlab.com/ee/user/project/requirements/?_gl=1*1qibz1u*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY0NjYyNC4xNTkuMC4xNjY2NjQ2NjQ0LjAuMC4w
              - name: Quality  management
                key: early
                link: https://docs.gitlab.com/ee/ci/test_cases/index.html?_gl=1*1qibz1u*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY0NjYyNC4xNTkuMC4xNjY2NjQ2NjQ0LjAuMC4w
      - stages:
          - name: Create
            features:
              - name: Source code management (SCM)
                key: mature
                link: https://about.gitlab.com/stages-devops-lifecycle/source-code-management/
              - name: Code review
                key: mature
                link: https://about.gitlab.com/stages-devops-lifecycle/code-review/
              - name: Snippets
                key: mature
                link: https://docs.gitlab.com/ee/user/snippets.html?_gl=1*1110vzc*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY0NjYyNC4xNTkuMC4xNjY2NjQ2NjQ0LjAuMC4w
              - name: Wiki
                key: mature
                link: https://docs.gitlab.com/ee/user/project/wiki/?_gl=1*1110vzc*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY0NjYyNC4xNTkuMC4xNjY2NjQ2NjQ0LjAuMC4w
              - name: Web IDE
                key: mature
                link: https://docs.gitlab.com/ee/user/project/web_ide/index.html?_gl=1*1110vzc*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY0NjYyNC4xNTkuMC4xNjY2NjQ2NjQ0LjAuMC4w
              - name: Pages
                key: mature
              - name: Git LFS
                key: early
              - name: Remote development
                key: planned
          - name: Verify
            features:
              - name: Continuous integration (CI)
                key: mature
                link: https://about.gitlab.com/features/continuous-integration/
              - name: Code testing and coverage
                key: mature
                link: https://docs.gitlab.com/ee/ci/unit_test_reports.html?_gl=1*sst1r9*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
              - name: Merge trains
                key: mature
                link: https://docs.gitlab.com/ee/ci/pipelines/merge_trains.html?_gl=1*13ku7z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
              - name: Review apps
                key: mature
                link: https://about.gitlab.com/stages-devops-lifecycle/review-apps/
              - name: Accessibility testing
                key: mature
              - name: Performance testing
                key: early
                link: https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html?_gl=1*13ku7z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
              - name: Secrets management
                key: early
                link: https://docs.gitlab.com/ee/ci/secrets/?_gl=1*13ku7z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
      - stages:
        - name: Secure
          features:
            - name: SAST
              key: mature
              link: https://docs.gitlab.com/ee/user/application_security/sast/?_gl=1*13ku7z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: DAST
              key: mature
              link: https://docs.gitlab.com/ee/user/application_security/dast/?_gl=1*13ku7z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Fuzz testing
              key: mature
              link: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/?_gl=1*13ku7z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Secret detection
              key: mature
              link: https://docs.gitlab.com/ee/user/application_security/secret_detection/?_gl=1*13ku7z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Container scanning
              key: mature
              link: https://docs.gitlab.com/ee/user/application_security/container_scanning/?_gl=1*vxdmne*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Dependency scanning
              key: mature
              link: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/?_gl=1*vxdmne*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: API Security
              key: mature
              link: https://docs.gitlab.com/ee/user/application_security/dast_api/?_gl=1*vxdmne*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Code quality
              key: early
              link: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html?_gl=1*vxdmne*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: License compliance
              key: early
              link: https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html?_gl=1*vxdmne*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
        - name: Package
          features:
            - name: Package registry
              key: mature
              link: https://docs.gitlab.com/ee/user/packages/package_registry/?_gl=1*vxdmne*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Dependency proxy
              key: mature
              link: https://docs.gitlab.com/ee/user/packages/dependency_proxy/?_gl=1*vxdmne*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Container registry
              key: mature
              link: https://docs.gitlab.com/ee/user/packages/container_registry/?_gl=1*vxdmne*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Helm chart registry
              key: mature
              link: https://docs.gitlab.com/ee/user/packages/container_registry/?_gl=1*vxdmne*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w#use-the-container-registry-to-store-helm-charts
            - name: Dependency firewall
              key: planned
        - name: Release
          features:
            - name: Continuous delivery (CD)
              key: mature
              link: https://about.gitlab.com/features/continuous-integration/
            - name: Advanced deployments
              key: mature
              link: https://docs.gitlab.com/ee/topics/autodevops/index.html?_gl=1*6ffcxc*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w#incremental-rollout-to-production-premium
            - name: Feature flags
              key: mature
              link: https://docs.gitlab.com/ee/operations/feature_flags.html?_gl=1*6ffcxc*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Release orchestration
              key: mature
              link: https://docs.gitlab.com/ee/user/project/releases/?_gl=1*6ffcxc*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Release evidence
              key: early
              link: https://docs.gitlab.com/ee/user/project/releases/?_gl=1*6ffcxc*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w#release-evidence
            - name: Environment management
              key: early
              link: https://docs.gitlab.com/ee/ci/environments/?_gl=1*6ffcxc*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
      - stages:
        - name: Configure
          features:
            - name: Auto DevOps
              key: mature
              link: https://about.gitlab.com/stages-devops-lifecycle/auto-devops/
            - name: Kubernetes management
              key: mature
              link: https://about.gitlab.com/solutions/kubernetes/
            - name: Infrastructure as code
              key: mature
              link: https://docs.gitlab.com/ee/user/infrastructure/iac/index.html?_gl=1*6ffcxc*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Deployment management
              key: early
              link: https://docs.gitlab.com/ee/topics/release_your_application.html?_gl=1*6ffcxc*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: ChatOps
              key: early
              link: https://docs.gitlab.com/ee/ci/chatops/?_gl=1*1gnsdm9*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Cluster cost management
              key: planned
        - name: Monitor
          features:
            - name: Incident management
              key: mature
              link: https://docs.gitlab.com/ee/operations/incident_management/?_gl=1*1gnsdm9*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Metrics
              key: early
              link: https://docs.gitlab.com/ee/operations/metrics/?_gl=1*1gnsdm9*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Error tracking
              key: early
              link: https://docs.gitlab.com/ee/operations/error_tracking.html?_gl=1*1gnsdm9*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: On-call schedule management
              key: early
              link: https://docs.gitlab.com/ee/operations/incident_management/oncall_schedules.html?_gl=1*1gnsdm9*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Continuous verification
              key: early
            - name: Logging
              key: planned
            - name: Tracing
              key: planned
              link: https://docs.gitlab.com/ee/operations/tracing.html?_gl=1*1gnsdm9*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
        - name: Govern
          features:
            - name: Vulnerability management
              key: mature
              link: https://docs.gitlab.com/ee/user/application_security/security_dashboard/?_gl=1*1ogxv6z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Audit Events
              key: mature
              link: https://docs.gitlab.com/ee/administration/audit_events.html?_gl=1*1ogxv6z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Compliance Management
              key: mature
              link: https://docs.gitlab.com/ee/administration/compliance.html?_gl=1*1ogxv6z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Dependency Management
              key: mature
              link: https://docs.gitlab.com/ee/user/application_security/dependency_list/?_gl=1*1ogxv6z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w
            - name: Security policy management
              key: early
              link: https://docs.gitlab.com/ee/user/application_security/?_gl=1*1ogxv6z*_ga*MTA4NDQxNDkyOS4xNjQyNzk4Mzcy*_ga_ENFH3X7M5Y*MTY2NjY1NDUwOC4xNjAuMS4xNjY2NjU1ODAzLjAuMC4w

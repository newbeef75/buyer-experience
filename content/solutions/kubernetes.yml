---
  title: Integrate Kubernetes to your DevOps Lifecycle
  description: GitLab’s Kubernetes Integration lets you build, test, deploy, and run your app at scale.
  components:
    - name: 'solutions-hero'
      data:
        title: Kubernetes + GitLab
        subtitle: Everything you need to build, test, deploy, and run your app at scale
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: /webcast/scalable-app-deploy/
          text: See how GitLab can help your team scale app deployment!
          data_ga_name: scale app deployment
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: gitlab + kubernetes"
    - name: copy-media
      data:
        block:
          - header: The best solution for cloud native development
            miscellaneous: |
              Cloud native applications are the future of software development. Container-packaged, dynamically managed, and microservice-oriented, cloud native systems enable faster development velocity while maintaining operational stability.

              GitLab is a single application with everything you need for [end-to-end software development and operations](/stages-devops-lifecycle/){data-ga-name="devops lifecycle" data-ga-location="body"}. From issue tracking and source code management to CI/CD and monitoring, having it all in one place simplifies toolchain complexity and speeds up cycle times. With a [built-in container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html){data-ga-name="container registry" data-ga-location="body"} and [Kubernetes integration](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="kubernetes integration" data-ga-location="body"}, GitLab makes it easier than ever to get started with containerization and cloud native development, as well as optimizing your cloud app development processes.
          - header: What is Kubernetes?
            miscellaneous: |
              Kubernetes is an open source container orchestration platform. It is designed to automate your management of application containers, from deploying and scaling to operating. Kubernetes orchestration allows you to partition as you go scaling up and down as necessary. You can respond quickly and efficiently to customer demand while limiting hardware usage within your production environment and minimizing disruption to feature rollouts.
            media_link_href: /blog/2017/11/30/containers-kubernetes-basics/
            media_link_text: Learn more about Kubernetes
            media_link_data_ga_name: more about kubernetes
            media_link_data_ga_location: body
          - header: Deploy GitLab on Kubernetes or use GitLab to test and deploy your software on Kubernetes
            miscellaneous: |
              GitLab works with or within Kubernetes in three distinct ways. These can all be used independently or together.

              * [Deploy software from GitLab to Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="deploy with kubernetes" data-ga-location="body"}
              * [Use Kubernetes to manage runners attached to your GitLab instance](https://docs.gitlab.com/runner/install/kubernetes.html){data-ga-name="kubernetes runners" data-ga-location="body"}
              * [Run the GitLab application and services on a Kubernetes cluster](https://docs.gitlab.com/charts/){data-ga-name="kubernetes cluster" data-ga-location="body"}

              Each approach outlined above can be used with or without the others. For example, an omnibus GitLab instance running on a virtual machine can deploy software stored within it to Kubernetes through a docker runner.
          - header: Kubernetes Integration
            inverted: true
            text: |
              While you can use GitLab to deploy your apps almost anywhere from bare metal to VMs, GitLab is designed for Kubernetes. [The Kubernetes integration](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="kubernetes integration" data-ga-location="body"} gives you access to advanced features like:

              * [Pull-based deployments](https://docs.gitlab.com/ee/user/clusters/agent/repository.html#synchronize-manifest-projects){data-ga-name="pull-based deployments" data-ga-location="body"}
              * [Deploying from the GitLab CI/CD through a secure connection](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_tunnel.html){data-ga-name="CI/CD secure connection" data-ga-location="body"}
              * [Canary Deployments](https://docs.gitlab.com/ee/user/project/canary_deployments.html){data-ga-name="canary deployments" data-ga-location="body"}
              * [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/){data-ga-name="auto devops" data-ga-location="body"}
            icon:
              name: kubernetes
              alt: Kubernetes
              variant: marketing
              hex_color: '#0E71F3'
          - header: Use GitLab to test and deploy your app on Kubernetes
            text: |
              [GitLab CI/CD](/features/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"} lets you easily manage deployments to multiple environments. Run automated tests in parallel with auto-scaling [GitLab Runners](https://docs.gitlab.com/runner/){data-ga-name="gitlab runners" data-ga-location="body"}. Manually test changes in a live, production-like environment before merging code with Review Apps. Runners, [Reviews Apps](/stages-devops-lifecycle/review-apps/){data-ga-name="review apps" data-ga-location="body"} and your own application can be deployed to your Kubernetes cluster or any environment you choose.
            icon:
              name: rocket-alt
              alt: Rocket
              variant: marketing
              hex_color: '#F43012'
            link_href: /partners/technology-partners/google-cloud-platform/
            link_text: Deploy on Google Cloud Platform
            link_data_ga_name: deploy on GCP
            link_data_ga_location: body
          - header: Use the same tool that tests Kubernetes
            inverted: true
            text: |
              Prometheus, CoreDNS, and Kubernetes itself are built, deployed, and tested by [CNCF](https://www.cncf.io/) using GitLab. They deploy multiple projects to multiple clouds with end-to-end testing using GitLab multi-project deploy boards to monitor progress.
            video:
              video_url: https://www.youtube.com/embed/Jc5EJVK7ZZk?start=372&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
            link_href: /customers/cncf/
            link_text: Learn More
            link_data_ga_name: cncf
            link_data_ga_location: body
    - name: featured-media
      data:
        column_size: 3
        horizontal_rule: true
        media:
          - title: Auto DevOps Documentation
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-devops.png
              alt: "Devops"
            link:
              text: Read more
              href: https://docs.gitlab.com/ee/topics/autodevops/index.html
              data_ga_name: DevOps documentation
              data_ga_location: body
          - title: Create CI/CD Pipeline with Auto Deploy
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm.png
              alt: "Create CI/CD Pipeline Blog Post"
            link:
              text: Read more
              href: /blog/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/
              data_ga_name: CI/CD with auto deploy
              data_ga_location: body
          - title: Install GitLab on Kubernetes
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-gitlab-kubernetes.png
              alt: "Create CI/CD Pipeline Blog Post"
            link:
              text: Read more
              href: https://docs.gitlab.com/charts/
              data_ga_name: install gitlab on kubernetes
              data_ga_location: body
          - title: Cloud Native Webinar
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-webcast.png
              alt: "GitLab Webcast"
            link:
              text: Watch now
              href: /blog/2017/04/18/cloud-native-demo/
              data_ga_name: cloud native webinar
              data_ga_location: body
